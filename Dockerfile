FROM node:16 as builder
WORKDIR /app

COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn

COPY . /app
RUN npm i -g http-server
RUN yarn build

EXPOSE 8080
CMD [ "http-server", "dist" ]
